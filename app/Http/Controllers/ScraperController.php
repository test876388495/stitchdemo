<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Channel1;
use App\Channel2;
//use Carbon\Carbon;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ScraperController extends Controller {


    //========================================================================================
    /*  
        set all touch records to 0 (useful for deleting old records)
        get all items from shoppify - save
        get all items from vend -save
        delete all records with touch = 0

    */

    public function syncUp() {


        //  SHOPIFY
        //$this->doShopifySyncUp();

        //  VEND
        $this->doVendSyncUp();


    }


    //========================================================================================

    public function doShopifySyncUp() {
        
        $locationId = '16173826110';
        $inventoryId = '19721779839038';

        //19337205022782
        $url = 'https://35e624dc2ef66234d8219b2092a1e52b:c28b8f2c00e3d16547fc3884d134ef42@stitch-code-challenge.myshopify.com/admin/inventory_levels/set.json';

        $headers = [       
            'Accept'        => 'application/json'
        ];
       
        //send get request to fetch data
        $payLoad = (object) [
            "inventory_item_id" => $inventoryId,
            "location_id" => $locationId,
            "available" => 1
        ];

        //$body = $payLoad;
        $body = json_encode($payLoad);
//dd($body);        
        $client = new Client();
        $response = $client->request('POST', $url, 
            [
                'headers' => $headers,
                'body' => $body
            ]
        );
 
        //check response status ex: 200 is 'OK'
        if ($response->getStatusCode() == 200) {


            //get body content
            $body =$response->getBody();

            $rawData = json_decode($body);
dd($rawData);            
           
        }

    }

    //========================================================================================

    public function doVendSyncUp() {
        
        $token = 'Kh7UKpuhyP5DqOG0BkHWn_6wb4nDhRwv18gfGfTl';
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Accept'        => 'application/json',
        ];

        $url = 'https://stitchvend.vendhq.com/api/products';

        //create new instance of Client class
        $client = new Client();

        $chan2s = Channel2::all();

        foreach($chan2s as $chan2) {

            $id = $chan2->vendProductId;
            $outletId = $chan2->outletId;
            $count = $chan2->quantity;

            $payLoad = [
                'id' => $id,
                'inventory' => [(object) [
                    'outlet_id' => $outletId,
                    'count' => $count
                ]]
            ];

            $body = json_encode($payLoad);


            $response = $client->request('POST', $url, 
                [
                    'headers' => $headers,
                    'body' => $body
                ]
            );
        }

        //dd($response);


    }


    //========================================================================================
    /*  
        set all touch records to 0 (useful for deleting old records)
        get all items from shoppify - save
        get all items from vend -save
        delete all records with touch = 0

    */

    public function sync() {

        $this->setTouch0();

        //  SHOPIFY
        $this->doShoppify();
        //$this->getShopifyLocationId();

        //  VEND
        $this->doVendProducts();
        $this->doVendInventory();

        $this->deleteOldStuff();

    }
    

    //========================================================================================
    /*
        touch, similar to unix command touch is used to track which records are active
        untouched records are deleted, because they have been deleted in Vend or Shopify
    */

    public function setTouch0() {

        Product::where('id', '>', -1)->update(array('touched' => 0));
        Channel1::where('id', '>', -1)->update(array('touched' => 0));
        Channel2::where('id', '>', -1)->update(array('touched' => 0));
              
    }


    //========================================================================================
    /*  delete untouched records
    */

    public function deleteOldStuff() {

        $products = Product::where('touched', 0)->get();
//dd($products);        
        foreach($products as $product) $product->delete();

        $variants = Channel1::where('touched', 0)->get();
        foreach($variants as $variant) $variant->delete();

        $variants = Channel2::where('touched', 0)->get();
        foreach($variants as $variant) $variant->delete();


    }


    //========================================================================================
    /*
        call Vend and get product info
    */

    public function doVendProducts() {

        //     GET INVENTORY FOR VEND  ===============================================
        $url = 'https://stitchvend.vendhq.com/api/2.0/products';

        $response = $this->callVend($url);

        //check response status ex: 200 is 'OK'
        if ($response->getStatusCode() == 200) {
 
            //get body content
            $body =$response->getBody();

            $rawData = json_decode($body);
            //dd($products);
            $products = $rawData->data;

            
            //  BUG in VEND?  Not able to see first item "Discount" on their site, but it
            //  shows up on the API call.

            $bugWorkaround = 0;

//dd($products);
            foreach($products as $variant) {
                if($bugWorkaround == 0) {
                    $bugWorkaround++;
                    continue;
                }

//dd($product);

                $productName = $variant->name;
            

                //  see if it exists in DB
                $sku = $variant->sku;
                $price = $variant->price_including_tax;
                $vendProductId = $variant->id;
                $sub = '';
                $mainNameLen = strlen($variant->name);
                $sub = substr($variant->variant_name, $mainNameLen+3);
//dd($sub);
                //$quantity = $variant->inventory_quantity;

                $prodObj = Product::where('productName', $productName)->first();
                if($prodObj == null) $prodObj = new Product();
                $prodObj->productName = $productName;
                $prodObj->touched = 1;

                $channels = $prodObj->channels;
                if($channels == null) $channels = '';
                $channels = explode(' ', $channels);
                $channelAlreadyIn = in_array('Vend', $channels);

                if(!$channelAlreadyIn) {
                    $stTem = $prodObj->channels.' Vend';
                    $prodObj->channels = trim($stTem);
                }


                $prodObj->save();

                $varObj = Channel2::where('sku', $sku)->first();
                if($varObj == null) $varObj = new Channel2();
                //dd($varObj['id']);

                $varObj->productId = $prodObj->id;
                $varObj->sku = $sku;
                $varObj->price = $price;
                $varObj->sub = $sub;
                $varObj->touched = 1;
                $varObj->vendProductId = $vendProductId;
                $varObj->save();

            }

        }
    }

    //========================================================================================
    /*
        call Vend iventory, match by productId and update inventory
    */
    public function doVendInventory() {

        //     GET INVENTORY FOR VEND ==============================================
        $url = 'https://stitchvend.vendhq.com/api/2.0/inventory';

        $response = $this->callVend($url);

        //check response status ex: 200 is 'OK'
        if ($response->getStatusCode() == 200) {
 
            //get body content
            $body =$response->getBody();

            $rawData = json_decode($body);
            
            $inventorys = $rawData->data;
//dd($inventorys);
         
            foreach($inventorys as $inventory) {
               
//dd($variant);

                $vendProductId = $inventory->product_id;
                $varObj = Channel2::where('vendProductId', $vendProductId)->first();

                if($varObj == null) continue;

                $varObj->quantity = $inventory->inventory_level;
                $varObj->outletId = $inventory->outlet_id;
                $varObj->save();

            }

        }
        


    }

    //========================================================================================
    /*
     helper function that makes API call to Vend
    */
    public function callVend($url) {

        $token = 'Kh7UKpuhyP5DqOG0BkHWn_6wb4nDhRwv18gfGfTl';
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Accept'        => 'application/json',
        ];


        //create new instance of Client class
        $client = new Client();

        $response = $client->request('GET', $url, 
            ['headers' => $headers]
        );

        return $response;
    }


    //========================================================================================

    public function getShopifyLocationId() {

        $url = 'https://35e624dc2ef66234d8219b2092a1e52b:c28b8f2c00e3d16547fc3884d134ef42@stitch-code-challenge.myshopify.com/admin/inventory_levels.json?inventory_item_ids=19337205022782';

        $client = new Client();
        //send get request to fetch data
        $response = $client->request('GET', $url);
 
        //check response status ex: 200 is 'OK'
        if ($response->getStatusCode() == 200) {


            //get body content
            $body =$response->getBody();

            $rawData = json_decode($body);
dd($rawData);            
            $products = $rawData->products;
dd($products);
        }
    }


    //========================================================================================
    /*
        Get product and inventory info from Shopify
    */
    public function doShoppify() {

        $url = 'https://35e624dc2ef66234d8219b2092a1e52b:c28b8f2c00e3d16547fc3884d134ef42@stitch-code-challenge.myshopify.com/admin/products.json';

        $client = new Client();
        //send get request to fetch data
        $response = $client->request('GET', $url);
 
        //check response status ex: 200 is 'OK'
        if ($response->getStatusCode() == 200) {


            //get body content
            $body =$response->getBody();

            $rawData = json_decode($body);
            $products = $rawData->products;
//dd($products);
            foreach($products as $product) {
                $productName = $product->title;
                $variants = $product->variants;
//dd($variants);
                foreach($variants as $variant) {
                    //  see if it exists in DB
                    //dd($variant);
                    //dd($variant->sku);
                    $inventory_item_id = $variant->inventory_item_id;
                    $sku = $variant->sku;
                    $price = $variant->price;
                    $quantity = $variant->inventory_quantity;
                    $sub = $variant->title;

                    $prodObj = Product::where('productName', $productName)->first();
                    if($prodObj == null) $prodObj = new Product();
                    $prodObj->productName = $productName;


                    $channels = $prodObj->channels;
                    if($channels == null) $channels = '';
                    $channels = explode(' ', $channels);
                    $channelAlreadyIn = in_array('Shopify', $channels);

                    if(!$channelAlreadyIn) {
                        $stTem = $prodObj->channels.' Shopify';
                        $prodObj->channels = trim($stTem);
                    }
                    $prodObj->touched = 1;


                    $prodObj->save();

                    $varObj = Channel1::where('sku', $variant->sku)->first();
                    if($varObj == null) $varObj = new Channel1();
                    //dd($varObj['id']);

                    $varObj->productId = $prodObj->id;
                    $varObj->quantity = $quantity;
                    $varObj->sku = $sku;
                    $varObj->price = $price;
                    $varObj->sub = $sub;
                    $varObj->touched = 1;
                    $varObj->inventory_item_id = $inventory_item_id;
                    $varObj->save();
                }
                
                
            }
       


        } else {
            //  what should we do in case of error?
            //  send email to concerned?
        }
    }



    //========================================================================================
    /*
        Stitch Lite API to get product info
    */
    public function products($productId = '') {

        if($productId == '') {        //  get all products
            $outs = array();

            $products = Product::all();

            foreach($products as $product) {
                $outsTem = $this->productById($product->id);
                $outs = array_merge($outs, $outsTem);
            }

        } else {    //  get product for given sku
            $outs = $this->productById($productId);
            
        }

        echo json_encode($outs);
        exit;


    }


    //========================================================================================
    /*
        helper function to get product by productId
    */

    public function productById($productId) {
       
        $outs = [];

        $product = Product::find($productId);
        if($product == null) {
            echo 'Invalid productId';
            exit;
        }

        $channels = $product->channels;

        $channels = explode(' ', $channels);

        foreach($channels as $channel) {
            $variants = array();
            $channelName = '';
            if($channel == 'Shopify') {
                $variants = Channel1::where('productId', $productId)->get();
            } else if($channel == 'Vend') {
                $variants = Channel2::where('productId', $productId)->get();
            }
            foreach($variants as $variant) {
                $out = array();
                $out['name'] = $product->productName . ' '. $variant->sub;
                $out['channel'] = $channel;
                $out['price'] = $variant->price;
                $out['quantity'] = $variant->quantity;
                $out['sku'] = $variant->sku;

                array_push($outs, $out);
            }

        }
        return $outs;

    }


    //========================================================================================


	
}



