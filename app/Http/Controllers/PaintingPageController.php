<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Painting;
use App\Models\Utilities;
use App\Http\Controllers\HeaderController;

class PaintingPageController extends Controller {


	public function showPaintings() {
		// return View::make('page', array('name' => 'SVR'));
		//include_once('utils/UserLanguage.php');

		$util = new Utilities();
		$chin = $util->isChinese();
		$locale = $util->getLocale();

		app()->setLocale($locale);
		//echo trans('lang.plasticsPaintingTitle');

		$headerCont = new HeaderController();
		$imgSrc = $headerCont->makeHeader();

		 $data = array();
		 $data["headerImgSrc"] = $imgSrc;

		$data["chin"] = $chin;
		$menuCont = new Menu1Controller();
		$menu1texts = $menuCont->getMenu1texts();
		$data["menu1texts"] = $menu1texts;

		$menu2Cont = new Menu2Controller();
		$menu2texts = $menu2Cont->getPaintingMenu2texts();
		$data["menu2texts"] = $menu2texts;

		$series = 1;
		if(isset($_GET['series'])) $series = $_GET['series'];


		$userLang = new Utilities();
		$chin = $userLang->isChinese();

		//$paintingsRaw = Painting::all();
		$all = 0;
		if(isset($_GET['all'])) $all = $_GET['all'];

		if($all == 1) {
			$paintingsRaw = Painting::where('series', '=', "$series")->orderBy('gallery_seq', 'DESC')->get();
		} else {
			$paintingsRaw = Painting::where('series', '=', "$series")->limit('15')->orderBy('gallery_seq', 'DESC')->get();
		}


	
		//media LIKE '%lasti%'";
		//'name', '=', 'Lawly

		$paintings = array();
		$i = 0;

		foreach($paintingsRaw as $paint) {
			$paint->normalize();
			if($paint->gallery_seq <7) continue;

			$paint1 = array();
			$paint1["width"] = $paint->width;
			$paint1["height"] = $paint->height;
			$paint1["heightIn"] = $paint->heightIn;
			$paint1["widthIn"] = $paint->widthIn;

			$paint1["gallery_seq"] = $paint->gallery_seq;
			$paint1["image"] = $paint->image300;

			$paint1["title"] = '';
			$paint1["media"] = '';
			$paint1["chin"] = 0;

			$paint1["title"] = $paint->titleForLanguage;
			$paint1["media"] = $paint->mediaForLanguage;
			$paint1["availableOrSoldText"] = $paint->availableOrSoldText;
			$paint1["chin"] = $chin;

			$image[$i] = $paint->image;
			$paint1["statusSold"] = $paint->statusSold;
			$paint1["priceINR"] = $paint->priceINR;
			$paint1["priceUSD"] = $paint->priceUSD;
			$paint1["priceRMB"] = $paint->priceRMB;


			$paintings[$i] = $paint1;

			$i++;

		}

		if($series == 1) $data["pageTitle"] = trans('lang.oilsPaintingTitle');
		else if($series == 2) $data["pageTitle"] = trans('lang.plasticsPaintingTitle');
		else $data["pageTitle"] = trans('lang.expPaintingTitle');
		 $data["available"] = trans('lang.available');

		 $bottomButtonController = new BottomButtonController();
		$link = array();
		$text = array();
		 if($chin == 0) {
			 if($all == 1) {
				 $link[] = "aboutArtist.php";
				 $text[] = "About Artist";
			} else {
				$link[] = "series.php?series=".$series.'&all=1';
				$text[] = "See all paintings" ;
			}
			$link[] = "aboutArtwork.php";
			$text[] = "About Artwork";
		} else {
			 if($all == 1) {
				 $link[] = "aboutArtist.php?chin=1";
				 $text[] = "关于艺术家";
			} else {

				$link[] = "series.php?chin=1&series=".$series.'&all=1';
				$text[] = "查看全部作品";
			}
			$link[] = "aboutArtwork.php?chin=1";
			$text[] = "关于艺术";
		}


		 $bottomButtonLinks = $bottomButtonController->getTexts($text, $link);
		 $data["bottomButtonLinks"] = $bottomButtonLinks;

		$emailSignup = new EmailSignupController();
		$emailSignupTexts = $emailSignup->getTexts();

		$contactSection = new ContactSectionController();
		$contactSectionTexts = $contactSection->getTexts();






		 //return View::make('painting')->with('paintings', compact($pageTitle, $data));

		 return View::make('painting', compact('data', 'emailSignupTexts',
			'contactSectionTexts', 'paintings'));
		 //return view('painting', ['title'=>$title, 'width'=>$width, 'height'=>$height]);
	}

//----------------------------------------------------------------------------------



	public function portfolio() {
		// return View::make('page', array('name' => 'SVR'));
		//include_once('utils/UserLanguage.php');

		$util = new Utilities();
		$chin = $util->isChinese();
		$locale = $util->getLocale();

		app()->setLocale($locale);
		//echo trans('lang.plasticsPaintingTitle');

		$headerCont = new HeaderController();
		$imgSrc = $headerCont->makeHeader();

		 $data = array();
		 $data["headerImgSrc"] = $imgSrc;
		 $data["chin"] = $chin;

		$menuCont = new Menu1Controller();
		$menu1texts = $menuCont->getMenu1texts();
		$data["menu1texts"] = $menu1texts;

		$menu2Cont = new Menu2Controller();
		$menu2texts = $menu2Cont->getPaintingMenu2texts();
		$data["menu2texts"] = $menu2texts;

		$userLang = new Utilities();
		$chin = $userLang->isChinese();

		//$paintingsRaw = Painting::all();
		// later on add back 2380, 2370, 2360
		$paintingPlasticIndex = ['2550', '2560', '2510', '2500', '2490', '2480'];

		$plastics = array();
		$i = 0;
		foreach($paintingPlasticIndex as $index1) {
			$paint = Painting::where('gallery_seq', '=', $index1)->first();

			if($paint == null) continue;

			$paint->normalize();
			$paint1 = array();
			$paint1["width"] = $paint->width;
			$paint1["height"] = $paint->height;
			$paint1["heightIn"] = $paint->heightIn;
			$paint1["widthIn"] = $paint->widthIn;

			$paint1["gallery_seq"] = $paint->gallery_seq;
			$paint1["image"] = $paint->image;

			$paint1["title"] = '';
			$paint1["media"] = '';
			$paint1["chin"] = 0;

			$paint1["title"] = $paint->titleForLanguage;
			$paint1["media"] = $paint->mediaForLanguage;
			$paint1["chin"] = $chin;

			$plastics[$i] = $paint1;

			$i++;

		}

		$paintingOilIndex = [770, 590, 580, 570, 540, 630, 1510, 1480, 2070, 1720, 1940, 1930, 230, 460];
		$oils = array();
		$i = 0;
		foreach($paintingOilIndex as $index1) {
			$paint = Painting::where('gallery_seq', '=', $index1)->first();

			$paint->normalize();
			$paint1 = array();
			$paint1["width"] = $paint->width;
			$paint1["height"] = $paint->height;
			$paint1["heightIn"] = $paint->heightIn;
			$paint1["widthIn"] = $paint->widthIn;

			$paint1["gallery_seq"] = $paint->gallery_seq;
			$paint1["image"] = $paint->image;

			$paint1["title"] = '';
			$paint1["media"] = '';
			$paint1["chin"] = 0;

			$paint1["title"] = $paint->titleForLanguage;
			$paint1["media"] = $paint->mediaForLanguage;
			$paint1["chin"] = $chin;

			$oils[$i] = $paint1;

			$i++;

		}




		 $data["pageTitle"] = trans('lang.portfolio');
		 $data["sunSeries"] = trans('lang.paintingMenu2sunSeries');
		 $data["starSeries"] = trans('lang.paintingMenu2starSeries');


		 $bottomButtonController = new BottomButtonController();
		$link = array();
		$text = array();
		 if($chin == 0) {
 			$link[] = "series.php?plastics=1";
			$text[] = "View Sun Series";
 			$link[] = "series.php?plastics=0";
			$text[] = "View Star Series";
		} else {
 			$link[] = "series.php?plastics=1&chin=1";
			$text[] = "看太阳系列画";
 			$link[] = "series.php?plastics=0&chin=1";
			$text[] = "看星星系列画";
		}


		 $bottomButtonLinks = $bottomButtonController->getTexts($text, $link);
		 $data["bottomButtonLinks"] = $bottomButtonLinks;

		$emailSignup = new EmailSignupController();
		$emailSignupTexts = $emailSignup->getTexts();

		$contactSection = new ContactSectionController();
		$contactSectionTexts = $contactSection->getTexts();

		 return View::make('portfolio', compact('data', 'emailSignupTexts',
			'contactSectionTexts', 'plastics', 'oils'));

	}

//====================================================================================


	public function rooms() {

		$util = new Utilities();
		$chin = $util->isChinese();
		$locale = $util->getLocale();

		app()->setLocale($locale);
		//echo trans('lang.plasticsPaintingTitle');

		$headerCont = new HeaderController();
		$imgSrc = $headerCont->makeHeader();

		 $data = array();
		 $data["headerImgSrc"] = $imgSrc;
		 $data["pageTitle"] = trans('lang.paintingMenu2closerLook');
		 $data["closeUpOfPainting"] = trans('lang.closeUpOfPainting');
		$data["chin"] = $chin;

		$menuCont = new Menu1Controller();
		$menu1texts = $menuCont->getMenu1texts();
		$data["menu1texts"] = $menu1texts;

		$menu2Cont = new Menu2Controller();
		$menu2texts = $menu2Cont->getPaintingMenu2texts();
		$data["menu2texts"] = $menu2texts;

		$userLang = new Utilities();
		$chin = $userLang->isChinese();

		$gallery_seq = 0;
		if(isset($_GET['index'])) $gallery_seq = $_GET['index'];

		$paint = Painting::where('gallery_seq', '=', $gallery_seq)->first();
		$paint->normalize();

		$paint1 = array();
		$paint1["width"] = $paint->width;
		$paint1["height"] = $paint->height;
		$paint1["heightIn"] = $paint->heightIn;
		$paint1["widthIn"] = $paint->widthIn;

		$paint1["gallery_seq"] = $paint->gallery_seq;
		$paint1["image"] = $paint->image;

		$paint1["title"] = '';
		$paint1["media"] = '';
		$paint1["chin"] = 0;

		$paint1["title"] = $paint->titleForLanguage;
		$paint1["media"] = $paint->mediaForLanguage;
		$paint1["available"] = $paint->available;
		$paint1["date"] = $paint->date;
		$paint1["availableOrSoldText"] = $paint->availableOrSoldText;
		$paint1["artistNotesForLanguage"] = $paint->artistNotesForLanguage;
		$paint1["chin"] = $chin;

		$paint1["statusSold"] = $paint->statusSold;
		$paint1["priceINR"] = $paint->priceINR;
		$paint1["priceUSD"] = $paint->priceUSD;
		$paint1["priceRMB"] = $paint->priceRMB;

		$paint1["closeUps"] = $paint->closeUps;
		//$paint1["closeUp1"] = $paint->closeUps[1];

		$paint1["rooms"] = $paint->rooms;

		$painting = $paint1;


		 $bottomButtonController = new BottomButtonController();
		$link = array();
		$text = array();
		 if($chin == 0) {
			$link[] = "aboutArtist.php";
			$text[] = "About Artist" ;
			$link[] = "aboutArtwork.php";
			$text[] = "About Artwork";
		} else {
			$link[] = "aboutArtist.php?chin=1";
			$text[] = "关于艺术家";
			$link[] = "aboutArtwork.php?chin=1";
			$text[] = "关于艺术";
		}


		 $bottomButtonLinks = $bottomButtonController->getTexts($text, $link);
		 $data["bottomButtonLinks"] = $bottomButtonLinks;

		$emailSignup = new EmailSignupController();
		$emailSignupTexts = $emailSignup->getTexts();

		$contactSection = new ContactSectionController();
		$contactSectionTexts = $contactSection->getTexts();

		 return View::make('rooms', compact('data', 'emailSignupTexts',
			'contactSectionTexts', 'painting'));

	}

//====================================================================================

	public function aboutArtwork() {
		// return View::make('page', array('name' => 'SVR'));
		//include_once('utils/UserLanguage.php');

		$util = new Utilities();
		$chin = $util->isChinese();
		$locale = $util->getLocale();

		app()->setLocale($locale);
		//echo trans('lang.plasticsPaintingTitle');

		$headerCont = new HeaderController();
		$imgSrc = $headerCont->makeHeader();

		 $data = array();
		 $data["headerImgSrc"] = $imgSrc;
		 $data["chin"] = $chin;

		$menuCont = new Menu1Controller();
		$menu1texts = $menuCont->getMenu1texts();
		$data["menu1texts"] = $menu1texts;

		$menu2Cont = new Menu2Controller();
		$menu2texts = $menu2Cont->getPaintingMenu2texts();
		$data["menu2texts"] = $menu2texts;

		$userLang = new Utilities();
		$chin = $userLang->isChinese();


		 $data["pageTitle"] = trans('lang.aboutArtwork');

		$texts = array();

//		if($chin==0) $texts[] = '<h3 style="text-align:center">Unique Contemporary Art for the Global Village</h3>';
//		else $texts[] = '<h3 style="text-align:center">为地球村而创作的艺术</h3>';

		$texts[] = '<img src="table1.jpg">';

		if($chin==0) $texts[] = '
<p>
"These paintings belong to a global ethnicity"
<br><br>

"One can see traces of Egyptian,
Chinese, Indian, European,
African and Arab influences
in Sridhar\'s work"
<br><br>
".. graduated from the Indian Institute of Technology - one of the <b>top</b> engineering universities in the world, worked in the US software industry for several years, went on a <b>10 month, 5000 mile</b> hike then moved to China.  We dream of doing things, he does it..."
</p>
';

else $texts[] = '<p>“这些作品属于全世界每一个民族。”
<br><br>
“从雷森明的作品中，我们可以看到埃及、中国、印度、欧洲、非洲以及阿拉伯艺术的痕迹。”
<br><br>
“从世界一流的理工大学--印度理工大学毕业,在美国做几年编程工作，然后徒步旅行10个月8000公里，最后来到中国。这都是我们梦寐以求的生活，而他--雷森明，已经做到了。”
<br><br>
</p>
';
	$texts[] = '<img src="images/set1.jpg" height="500">';


if($chin == 0) $texts[] = '<p>
"His artwork is meant for the <b>very</b> intelligent and <b>very</b> independent.  Commoners need not apply" \' Xiao Zhang
<br><br>
"Art that is playful at first glance, sucks you in, then makes you <b>think</b>."
<br><br>
"... artwork that is direct and <b>powerful</b>"
</p>
';
else $texts[] =  '<p>
“雷森明的创造力实在是太强了！”
<br><br>
“这些作品简约、直接、富有感染力。”
<br><br>
“我从未见过这样的作品！” - 宋伟， 中国的有名艺术家
</p>
';

$texts[] = '<img src="images/set2.jpg" height="500">
  <br>            	<br>
<img src="images/set3.jpg" height="500">';


if($chin==0) $texts[] =  '<p>
<b>PRESS</b>
<br><br>
...because he has lived in several countries over the years, his paintings depict a <b>merging</b> of different cultures - Messenger <b>Magazine</b>, University of Delaware, USA
<br><br>
"His art is <b>unconventional</b>. It reveals a romantic soul, expressed in a colorful and abstract way -
 Munchen Merkur <b>Newspaper</b>, Munich, Germany
 <br><br>
 "They are <b>unusual</b> examples of contemporary and modern art and have been exhibited in Japan, the United States, China, Mexico and India." Munchen Merkur <b>Newspaper</b>, Munich, Germany
</p>';
 else $texts[] =  '<p>
 <b>媒体评价：</b>
<br><br>
因为多年以来，他（雷森明）曾在多个国家生活过，所以他的作品描摹出了不同文化的融合。 --美国德拉华大学《信使》杂志
<br><br>
他的艺术是不同寻常的，通过多彩而抽象的表达，为我们展示了他那追求浪漫的灵魂。 --德国慕尼黑《慕尼黑塞报纸》
<br><br>
它们（雷森明的作品）是现当代艺术中与众不同的典范，曾在日本、美国、中国、墨西哥、印度等多地展出。 --德国慕尼黑《慕尼黑塞报纸》
</p>
';


$texts[] = '<img src="../blog/photos/img_1143.jpg" height="500">
  <br>          	<br>
<img src="../blog/photos/img_1169.jpg" height="500">
<br><br>
  <img src="../blog/photos/I0307b.jpg" height="500">
  <br>  <br>
<img src="../blog/photos/I0320b.jpg" height="500">';


if($chin==0) $texts[] =  '<p>
<b>Exhibitions:</b>
<br>
<b>China</b> - Lanzhou 2006, ZhangYe 2010 <b>Solo</b>, Nanchang 2010 <b>Solo</b>, Shanghai Contemporary 2011, Shanghai Art Expo 2011, Beijing 2012<br>
<b>Germany</b> - Munich 2011 <b>Solo</b><br>
<b>Japan</b> - Tokyo 2007, Tokyo 2009<br>
<b>Mexico</b> - Guadalajara 2008<br>
<b>USA</b> - Miami 2002, Ft. Lauderdale 2002, Austin 2006, Stamford 2011, <b>New York Art Expo</b> 2011<br>
</p>
';
else $texts[] =  '<p>
<b>画展：</b><br>
中国：兰州2006， 张掖个人画展2010，南昌个人画展2010，上海当代艺术展2011，上海艺术博览会2011，北京2012
<br>
德国：慕尼黑个人画展 2011
<br>
日本：东京2007，东京2009
<br>
墨西哥：瓜达拉哈拉2008
<br>
美国：迈阿密2002，劳德代尔堡2002，斯坦福2011，奥斯汀2006，纽约艺术博览会2011
</p>
';

$texts[] = '<img src="../blog/photos/D1612b.jpg" width="500">
<br><br>
  <img src="../blog/photos/img_1276.jpg" height="500">
  <br><br>
<img src="../blog/photos/img_1282.jpg" height="500">';


if($chin==0) $texts[] = '<p>
<b>PROFESSIONAL CRITIQUES:</b><br><br>
<i>
...have a VERY visually distinctive and <b>attractive</b> technique and style. These works instantly grab one\'s attention, and then demand a second, closer work</i><br> - David Huang
<br><br>
<i>
...found them to be quite <b>compelling</b>, fresh...ordered and well orchestrated. The color holds interest.. </i><br> - Larry Seiler
<br><br>
<i>
...work is utterly <b>charming</b>. I love the consistent theme and process of the work. It is a solid thoughtful unit. The style is very contemporary and playful. Whimsical and fun</i><br> - Linda Blondheim
<br><br>
<i>
-it has immediate visual impact and weight, the compositions are <b>bold</b> in their use of strong, clear shapes, and also complex as the the shapes mesh, interlock, overlap in <b>inventive</b> ways. Overall there is a clear sense of an individual, personal approach</i><br> - Rebecca Caldwell
</p>
';
else $texts[] =  '<p>
<b>专业评论：</b>
<br>
（雷森明的作品）在技巧和风格方面塑造了非常独特和迷人的视觉效果，这些作品能迅速抓住人们的注意力，然后促使人们仔细去品味、观赏。--David Huang
<br><br>
我发现这些作品非常引人注目并且富有新意，结构有序而协调，色彩的运用激发起人们的兴趣。--Larry Seiler
<br><br>
（雷森明的）作品极度迷人，我喜欢其中始终如一的主题以及作品的制作工序，它们引人深思，风格具有当代特征且富有趣味，是怪诞与乐趣的结合。--Linda Blondheim
<br><br>
这些作品有很强的视觉冲击力，其中强烈而清晰的形状运用非常大胆，不同的形状互相啮合、交织、重叠，虽然复杂却富有创造力。总体来说，这其中彰显了独立的个人创作方法。 -- Rebecca Caldwell
</p>
';


$texts[] = ' <img src="../blog/photos/DSC01455.jpg" width="500"><br>
  <br>
<img src="../blog/photos/img_1530.jpg" width="500">';



if($chin==0) $texts[] = '<p>
<b>TEXTURE AND MATERIALS:</b><br><br>
Sridhar is an engineer as well as an artist.  He frequently uses engineering materials in his artwork
and develops his own techniques.
Polyart uses different forms of plastics such as PVC, PMMA, PLA, resin, acrylic paints and so on.
The technique was invented by Sridhar and uses a CNC router and a 3d printer assembled by the artist.
The artwork have a transparent, floating feel.  Here are some photos:
</p>
';
else $texts[] =  '<p>
<b>结构与材料：</b><br><br>
很多作品都有着丰富的“内涵”
<br><br>
雷森明运用不同的材料与载体，包括丙烯与颜料，画布、纸、塑料和金属纱窗等等。
<br><br>
“我从未见过如此的作品，他们看起来通透、洁净，非常现代化。”--著名中国画家宋伟对雷森明金属纱窗画的评价。
</p>
';

$texts[] = '<img src="../images/detail5.jpg" height="500"><br><br><br>
<img src="../images/detail6.jpg" height="500"><br><br><br>
<img src="../images/detail4.jpg" width="500"><br><br><br>
<img src="roomShots/mangoTreeRM1.jpg" height="500"><br><br><br>
<img src="images/mangoTreeClose1.jpg" height="500"><br><br><br>
<img src="images/mangoTreeClose2.jpg" height="500">';


if($chin==0) $texts[] = '<p>
Paintings for the Living room, Bed room, Hall, Bar, Office .....
<br><br>
<b>Unique and bold</b> paintings for the <b>unique and bold</b>.
<br><br>
<b>Purchase online or visit our gallery in Beijing.</b>
</p>';
else $texts[] =  '<p>
作品可用作装饰客厅、卧室、走廊、酒吧和办公室。
<br><br>
为独特而勇敢的人们所创作的独特而大胆的作品。
<br><br>
欢迎在线购买，或前往北京的画廊参观。
</p>
<center>

';

$texts[] = '<img src="livingRooma.jpg"><br>
  <br>
<img src="livingRoom2a.jpg">';


$texts[] = '<img src="livingRoom2a.jpg" alt="">';




 	if($chin == 0) $texts[] =  '

<p>
 	<b>Prices</b><br><br>
 	Paintings here are original, one of a kind.
 	They are not copies or factory  produced "artwork".  You can expect to pay between 500 to 5000
 	USD per painting.  Smaller = cheaper.  Paintings are shipped all over the world.  Contact
 	us for prices in your currency.
 	<br><br>
 	Every once in a while, we have prints or small studies that are available for a cheaper price.
 	Please contact for availability.
 	</p>
 	';
 	else $texts[] =  '';

	if($chin == 0) $texts[] =
'
<p>
<b>HOW WILL YOUR PAINTING LOOK?</b><br>
<br>Below are some photographs showing the paintings in different settings. <br></p>
 <center>
 <img src="livingRooma.jpg">
 Living room made elegant with paintings "Cow in Pasture" and
"3 Searching".<br><br><br>
<img src="livingRoom2a.jpg">
Paintings "Couple", "Ill Fated Couple" and "Dancers and Dog".<br><br><br>
<img src="livingRoomb.jpg">
Paintings "Couple", "Ill Fated Couple" and "Dancers and Dog".<br><br><br>
<img src="table1.jpg">
Paintings "Talkers" and wooden table.<br><br><br>
<img src="table2.jpg">
Paintings "Dancers and Disapproving Elder" and wooden table.<br>
</center></p>';
 else $texts[] =  '<p>
<b>你期望画面看起来怎样？</b><br>
以下是一些照片用不同的设置显示的绘画作品

 <center>
<img src="livingRooma.jpg" alt="">
绘画使起居室更加优雅“奶牛在牧地”和“3个搜索”
<br><br><br>
<img src="livingRoom2a.jpg" alt="">
绘画“恋人”“注定要失败的情人”和“舞者和狗”
<br><br><br>
<img src="livingRoomb.jpg" alt="">
绘画“恋人”“注定要失败的情人”和“舞者和狗”
<br><br><br>
<img src="table1.jpg" alt="">
 绘画“健谈者”和“木制的桌”
<br><br><br>
<img src="table2.jpg" alt="">
绘画“舞者和反对的老者”和“木制的桌”

</center>
</p>
';




	if($chin == 0) $texts[] =  '
  <p>
  <b>Guarantee:</b><br><br>
  <b>Satisfaction guaranteed.</b>  If you are not satisfied, contact us within 10 days and we will
  return the money - no questions asked.  You only pay for the 2 way mailing.<br><br>
  We also guarantee that the materials used in the artwork are professional artist grade and
  should last many, many years.';
  else $texts[] =  '
  	<p>
  <b>满意度保证：</b>
   如果您不满意，请在10天内联系我，我们会毫无疑问地退还所有的钱，你仅需支付来返邮费，我们也保证用在艺术作品上的材料是专业艺术家等级的，能持续很多很多年。';

	if($chin == 0) $texts[] =  '
	<p>
  <b>Payment Options:</b><br><br>
	You can pay by Paypal, Cash or Personal Check.
<br><br>
  <b>Mailing Options:</b><br><br>

	We can send the painting by surface or air.  Of course,
    surface mailing is much cheaper than air but also a lot slower.
    All paintings except those on plexiglass are sent as a roll.
<br><br>
    On average international mailing cost for air per painting is 50 usd.  This is a rough estimate.
    Larger paintings are more expensive to mail.  An exact amount will be calculted before payment.
    </p>';
    else $texts[] =  '<p>
<b>支付方式</b><br><br>
你可以通过Paypal、现金和银行转账付款
</p>';



	if($chin == 0) $texts[] =  '	  <p>
  <b>Delivery Times:</b><br><br>
  Within China the delivery time is 2-4 days.  International air is 10 to 14 days.  International
  surface is 30 to 45 days.</p>';
    else $texts[] =  '<p><b>邮寄方式:</b><br><br>
我们可以通过海运和空运来寄画，当然，海运方式比空运方式更便宜，但也很慢。
    </p>';




	if($chin == 0) $texts[] =  '	  <p>
  <b>Discounts:</b><br><br>
  If purchasing multiple paintings, we can give you a discount.</p>';
    else $texts[] =  '<p><b>折扣:</b><br><br>
如果购买多个画，我们可以给你一个折扣</p>';




	if($chin == 0) $texts[] =  '	    <p>
    <b>Pricing and ordering details:</b><br><br>
  For prices, mailing options, payment options and any other questions, please <a href="/contact.php">
  contact</a>.  Your location and name of the painting are important, so please include them
  in your correspondence.</p>';
    else $texts[] =  '<p><b>价格和订购细节:</b><br><br>


对于价格，邮寄方式，支付方式和其它事项有任何疑问请和我联系。你的邮寄地址和所购买作品的名称是很重要的，请务必填写清楚。
</p>';




if($chin == 0) $texts[] =  '	  <p>
    <b>Free Brochure:</b><br><br>
  We can mail free brochures to customers anywhere in the world to help them in their decision making process.  Send us your request
  and street address and we will send it out to you.  Please remember to include your street address.  Click
  <a href="/contact.php">here</a>.</p><br><br>';
    else $texts[] =  '';



		 $bottomButtonController = new BottomButtonController();
		$link = array();
		$text = array();
		 if($chin == 0) {
			$link[] = "aboutArtist.php";
			$text[] = "About Artist" ;
			$link[] = "series.php";
			$text[] = "See Artwork";
		} else {
			$link[] = "aboutArtist.php?chin=1";
			$text[] = "关于艺术家";
			$link[] = "series.php?chin=1";
			$text[] = "艺术作品";
		}


		 $bottomButtonLinks = $bottomButtonController->getTexts($text, $link);
		 $data["bottomButtonLinks"] = $bottomButtonLinks;

		$emailSignup = new EmailSignupController();
		$emailSignupTexts = $emailSignup->getTexts();

		$contactSection = new ContactSectionController();
		$contactSectionTexts = $contactSection->getTexts();

		 return View::make('aboutArtwork', compact('data', 'emailSignupTexts',
			'contactSectionTexts', 'texts'));

	}

//-----------------------------------------------------------------------------------


	public function aboutArtist() {
		// return View::make('page', array('name' => 'SVR'));
		//include_once('utils/UserLanguage.php');

		$util = new Utilities();
		$chin = $util->isChinese();
		$locale = $util->getLocale();

		app()->setLocale($locale);
		//echo trans('lang.plasticsPaintingTitle');

		$headerCont = new HeaderController();
		$imgSrc = $headerCont->makeHeader();

		 $data = array();
		 $data["headerImgSrc"] = $imgSrc;
		$data["chin"] = $chin;

		$menuCont = new Menu1Controller();
		$menu1texts = $menuCont->getMenu1texts();
		$data["menu1texts"] = $menu1texts;

		$menu2Cont = new Menu2Controller();
		$menu2texts = $menu2Cont->getPaintingMenu2texts();
		$data["menu2texts"] = $menu2texts;

		$userLang = new Utilities();
		$chin = $userLang->isChinese();


		 $data["pageTitle"] = trans('lang.aboutArtist');

		$texts = array();


		if($chin == 0) $texts[] = '<p>



					<b>An Interview:</b><br><br>
<b>Sridhar, is a full time artist now living in Beijing. Sridhar, please give an introduction
of yourself.</b>
<br>
I am a US citizen of Indian origin. My full name is Sridhar Vallinayagam Ramasami - quite long.
My Chinese name is LeiSenMing. I am a full time artist living in Songzhuang artist village in
Beijing.
<br><br>

<b>You have an interesting story on how you became an artist. Tell us about it.</b>
<br>
Right. I was an engineer before I became an artist. I graduated in engineering from the Indian
Institute of Technology, the top engineering university in India. Then I got a scholarship for
 a master\'s degree in the USA, also in engineering. Then, I worked as an engineer and
 programmer for many years. But I found the corporate life too narrow and sterile. Some
  unexplainable force within me made me do a long hike. I hiked from Canada to Key West,
   USA - a distance of 8000 kilometers in 10 months. After the hike, I realized that there
    are other things besides money that are more important in life. A few months later, I
    moved to China and became an artist.
<br><br>


<img src="../images/artist2.jpg">



<br>

<b>That\'s a very long hike. Tell us more about it</b>.
<br>
The hike was a spiritual experience for me. I discovered that society, TV, friends, family
etc are always telling us something. Sometimes directly, many times indirectly. It puts us
in a trance, that is very difficult to break. But, when you go into a forest that noise is
cut off. After about 6 months, one can think clearly. The great sages of the world like Buddha,
Christ, Mohammad etc all went into the forests or deserts to contemplate. There is an excellent
 reason they did that. A long silence is essential to break society\'s trance and to find oneself.
<br><br>

<b>How about the dangers of such a long hike?</b>
<br>
The danger is far less than one imagines - that is the other thing I discovered on the
long hike. Most of our fears are in our heads. Sure, there are dangers when you are out
alone in a remote forest, but the perceptions are blown out of proportion. Bears - they are hunted by human beings, so they run when they see human beings. Snakes - when you sleep, don\'t sleep in a rocky place with many holes. Cold - there are survival common sense tactics one can use when it\'s very cold. Getting lost - if you are on a mountain, walk down. If on level ground, keep walking in the same direction and you are guaranteed to run into some road.
<br><br>

<b>What are some of the difficulties you faced on the trail?</b>
<br>
Extreme cold. There were nights when the shoes would freeze solid and it would very difficult
to wear the next morning. After putting the feet in them, the feet would be extremely cold
and painful for about half an hour. Setting up tent and cooking at the end of the day\'s
 hike - set up the tent, get into the sleeping bag and then cook. Otherwise, it\'s just
  too cold. Then there were the more common difficulties of getting lost, hunger, thirst etc.
<br><br>

<b>Now let\'s talk about your art - it\'s very different from most normal art. Give us an
 introduction.</b>
<br>
My art is contemporary, in the true sense of the word. What that means is that it represents
 the influences of the current world. Mona Lisa may be a wonderful painting, but it represents
  the influences of the world 500 years ago. What is happening now in the world - more inter
  cultural exchanges, new materials, new colors, new design concepts, more abstract ideas.
   All these are reflected in my paintings. As an example, if you look at the painting \'Four
    Trees\' - the trees are abstracted and simplified. The clouds above are also simplified.
     The design of the clouds has a Chinese influence. The design of the trees has an Indian
      influence and the abstractness has a US influence. If you look at the painting Musician
       - the materials are modern materials. The painting is on a wire mesh. It uses acrylics,
        plastics and resins. This painting could not have been made 100 years ago. So what I
         am trying to do is use present day materials and ideas and create something that is
          beautiful.
<br><br>


<img src="images/fourTrees.jpg">
<br><br>
<img src="images/musicianSymbol.jpg">

<br><br>

<b>Let\'s look at this painting Fashionable Women. Introduce that one to us.</b>
<br>

Sure. There are three figures in the paintings. They are three fashionable women. They are
standing in different poses. They design of the figures has a Chinese influence, while the
 design of the clothes has an Indian influence. The abstractness and the divisions of the chest,
  body and head has a US influence. You can see some letters on the left. They are abstract and
  don\'t belong to any language. The painting depicts the poise, confidence and beauty of runway
   models.
<br><br>

<img src="/painting/images/fashion.jpg">

<br><br>

<b>You also work with unconventional but common materials. Tell us about that.</b>
<br>
I love experimenting with all kinds of stuff. I have experimented with paintings on wire mesh.
The wire mesh has a very transparent feel and then I paint on it using plastics, acrylics and
other compatible materials. So, these paintings have a completely different feel from canvas
paintings. An example is the Musician painting. I have also painted on Plexiglas using acrylics,
plastics and resins. Flower Pot and Plant is an example.
<br><br>

<b>How about the longevity of such materials?</b>
<br>
I have chosen the material carefully. They are compatible with each other. Most have a plastic
base, so they match and as you know plastic lasts much, much longer than canvas or cloth. So,
the artwork I produce is designed to last a long time. Additionally, I guarantee the stability
 of the artwork for ten years. So the collector can rest at ease.
<br><br>

<b>What kind of people buy your art?</b>
<br>
Over time I have found that 90% of my customers have at least a Master\'s degree, have multi
national lives by either living in different countries or being married to people from other
countries or owning companies abroad. Also, most of my collectors are very successful and are
 trend setters in their own fields.
<br><br>

<b>So, small town folk don\'t buy your painting?</b>
<br>
They do. But they tend to be unusual people. Most are entrepreneurs - very talented,
confident and successful.
<br><br>

<b>You have exhibited in many countries. Which ones?</b>
<br>
I have exhibited in the USA, China, India, Mexico, Japan and Germany.
<br><br>

<b>What are some of your memorable exhibitions?</b>
<br>
The big, famous exhibitions were the Art Expo in New York, Shanghai Contemporary and Shanghai
 Art Expo. But unusual or special exhibitions - I had one in a small village in Gansu. There
 was a fair and some of my paintings were put on a desk and were leaning on sticks. The villagers
  were very curious. Another interesting one, was my first solo exhibition. It was in ZhangYe.
   It was the first time I saw my artwork fill up a large exhibition hall. I found talking to
   the visitors very interesting. Another interesting exhibition was a solo exhibition in my
    alma mater - Indian Institute of Technology, Kharagpur. That was the first time, my class
     mates got to see my paintings first hand. It was quite overwhelming.
<br><br>

<b>How can one buy your paintings?</b>
<br>
In some cities, my paintings are available in galleries. The collector can drop by these
galleries and talk directly with them. If there is no gallery in the city with my work,
 the collector can visit my website and choose the paintings.
<br><br>

<b>Mr. Sridhar thanks for your time and good luck.</b>
<br>
Thanks.
</p>    ';
else $texts[] = '<p>
				<b>一个面试：</b><br><br>

问：雷森明先生是一位专职画家，现居于北京。雷森明先生您好，请您先简单介绍一下您自己吧。
<br><br>
答：好的，我是一名美国公民，出生于印度。我的全名是“思瑞达·威利那雅葛姆·拉姆萨米”（有点长），中文名字是雷森明。我是一名专职画家，现在住在位于北京的宋庄艺术家村。

<img src="/images/artist2.jpg">

<br><br>
问：听说您成为画家的过程背后隐藏着一个非常有趣的故事，能为我们详细讲述一下吗？
<br><br>
答：好的。在成为一名画家之前，我是一名工程师。我毕业于印度理工学院，所学的专业是工程学，印度理工学院是印度工科院校中的最高学府。毕业后，我拿到了美国一所大学的全额奖学金，开始攻读硕士学位，专业同样是工程学。之后的很多年，我一直从事工程和计算机编程方面的工作，但是在公司工作的过程当中，我愈发觉得自己的生活过于狭隘且没有营养，内心深处产生的一种莫名的力量驱使我抛开一切，开始了一段很长的徒步旅行。我从加拿大开始，一直徒步走到了美国佛罗里达州的基韦斯特，全程8000公里，历时近10个月。经历了这次徒步旅行之后，我开始明白，除了金钱以外，生活中有更重要的东西。几个月以后，我来到了中国，开始进行艺术创作。
<br><br>
问：那确实是一段很长的徒步旅行，能告诉我们更多的细节吗？
<br><br>
答：那次徒步旅行对我来说是一种精神体验，我发现社会、电视、朋友、家人等都在向我们以直接或间接的方式传递着某种信息，这一现实将我们置于难以打破的幻境当中。但是，如果你走进一片森林，所有的噪音便消失无踪，坚持6个月以后，你便可以非常清晰地进行思考。历史中所有的圣贤之人，比如释迦牟尼、耶稣和穆罕默德等，他们都走入森林或者荒漠进行冥想沉思，他们之所以这么做，是因为长期的静默可以使人勘破尘世幻境，找到本我。
<br><br>
问：您在旅行过程当中遇到过危险吗？
<br><br>
答：其实旅途中所遇到的危险要比想象中的少很多——这也是我在旅途当中得出的另一结论。我们的很多恐惧只存在于脑海之中，诚然，如果你孤身一人行走在森林里，你会面临各种潜在的危险，但是你对危险的认知却被主观地夸大。比如，熊：熊会被人类猎杀，所以它们看到人类会自行躲开；蛇：如果你要睡觉，不要睡在乱石、洞穴密布的地方，就可以躲开蛇类；寒冷：人们有足够的常识与技巧来应对寒冷；迷路：如果你在山上，往下走就是，如果你在平地上，沿着同一方向一直走，你肯定能找到道路。
<br><br>
问：旅行过程中您都遇到了哪些困难？
<br><br>
答：极度的寒冷！有好几个晚上，我的鞋子都被冻得异常坚硬，以至于第二天早晨很难穿在脚上。而穿上鞋子以后，我的脚会被冻得发疼，半个多小时后才能缓解。每天的旅程结束后，我都需要支起帐篷，开始生火做饭。而每次我都是先支好帐篷，爬进睡袋，然后才开始做饭，不然就难以抵御寒冷。当然，我也遇到过其它困难，比如迷路、饥饿、口渴等。
<br><br>
问：好的，现在我们来谈一谈您的艺术创作。您的作品与大多数普通的艺术作品相比显得非常特别，能为我们介绍一下吗？
<br><br>
答：我的作品是真正意义上的现代艺术，这意味着我的作品反映了当今世界的影响。蒙娜丽莎是一件稀世名作，但它所表现的仅仅是500年前的世界。在当今的世界中，跨文化交流愈加频繁，新材料、新颜色、新设计理念和抽象思维不断涌现，而这所有的一切都被反映在了我的作品当中。举例来说，如果你看到我的作品《四棵树》，你会发现其中的树都是抽象和简单的，上面的云彩也是如此。云彩的设计是典型的中国风格，树木是印度风格，而画中的抽象性则属于美国风格。而我的另一幅作品《音乐家》，所使用的都是现代材料：整幅画在金属丝网上面创作完成，同时还使用了丙烯、塑料和树脂作为原料。而这幅作品在100年前根本无法创作产生。所以，我正在努力用现世存在的材料和理念去创造美好的事物。
<br><br>

<img src="images/fourTrees.jpg">
<br><br>
<img src="images/musicianSymbol.jpg">
<br><br>

问：您有一幅作品《时尚女郎》，能为我们介绍一下吗？
<br><br>
答：当然可以。这幅画当中有三个人物，三位时尚女郎以各不相同的姿势站立。这些人物的设计是典型的中国风格，但她们所穿的衣服却是印度风格，同样地，作品的抽象性，胸部、身体和头部的分割则具有典型的美国风格。在画的左边你可以观察到一些字母，它们只是抽象的符号，并不属于任何语种。整幅作品描摹出了T台模特的姿态、自信与美丽。
<br><br>

<img src="images/fashion.jpg">



<br><br>
问：您在作品中经常使用一些非常规却又很常见的材料，请简单介绍一下。
<br><br>
答：我喜欢用所有的材料来进行实验，我曾尝试在金属丝网上面创作，这种丝网给人一种透明的感觉，我用塑料、丙烯和其它能使用的材料在上面作画，创作出的作品与布面油画完全不同，我的作品《音乐家》便是一个很好的例子。我也使用丙烯、塑料和树脂在树脂玻璃上创作，《鲜花、盆与植物》便是这样产生的。
<br><br>
问：这些材料的寿命如何呢？
<br><br>
答：我所使用的材料都是经过精心挑选的，不同材料之间都能够完美结合。大部分材料都是可塑的，能够完全结合在一起，如你所知，塑料等聚合物要比帆布等织物的寿命更长，所以，我所设计的这些作品都具有很长的寿命。不仅如此，我还能够保证这些作品至少有10年时间的稳定性，所以收藏者们大可放心。
<br><br>
问：购买您作品的客户都是什么样的人呢？
<br><br>
答：长久以来，我发现购买我作品的客户当中，90%拥有硕士学位，他们中有的人在不同的国家生活，或者有过跨国工作、生活的经历；有的人与来自不同国家的伴侣结婚；有的人甚至在国外拥有自己的公司。并且，我的大多数客户都非常成功，在他们各自的领域都是引导潮流者！
<br><br>
问：那就是说，小城镇的居民不会购买您的作品了？
<br><br>
答：他们也会，但凡是购买我作品的客户都不是普通人，大多数这样的客户是企业家，他们都很有天赋，非常自信，也非常成功。
<br><br>
问：您曾在不同的国家参加或举办过画展，都有哪些国家呢？
<br><br>
答：我曾在美国、中国、印度、墨西哥、日本和德国参加、举办过画展。
<br><br>
问：哪些画展最具纪念意义呢？
<br><br>
答：规模较大、非常著名的画展，如纽约艺术博览会，上海当代艺术展和上海艺术博览会对我来说最具有纪念意义。当然，我也自己举办过一些特殊的画展，比如在甘肃的一个小乡村中，我将自己的作品带到了村里的集市上，将它们放在桌子上面，然后用棍子支了起来，引起了村民们的好奇。我在甘肃张掖举办的的首次个人画展也非常有趣，那是我第一次看到自己的作品挂满了整个展厅，也正是在那次画展上，我发现与参观者进行交流是一件多么有趣的事。另外一次个人画展是在我的母校——卡拉普印度理工学院举办的，在那次画展期间，我的同学们第一次近距离地看到了我的作品，画展的整个过程令我十分满意。
<br><br>
问：大家都是如何购买您的作品的？
<br><br>
答：在一些城市中，我的作品被陈列在画廊里，收藏者们可以直接去这些画廊，并与工作人员联系。如果您生活的城市中没有展示我作品的画廊，您可以访问我的个人网站并且挑选作品。
<br><br>
问：好的，非常感谢雷森明先生的参与，祝您好运！
<br><br>
答：谢谢！
</p>
';





	if($chin == 0) $texts[] = '<p>
<strong>PRESS</strong>
<br><br>
<b>Messenger April 3, 2011 - Newark, Delaware, USA</b>
<br><br>
<b>Painter exhibits at New York Art Expo</b><br>
Sridhar Ramasami, an engineer-turned-artist and a U.S. citizen with Indian roots who now lives
in China, exhibited his "One World" paintings at the New York Art Expo in March.<br>
<br>
Ramasami says that, because he has lived in several countries over the years, his paintings depict
a merging of different cultures.  When he became bored with his job as a computer programmer,
he decided to change his life and took a 10-month, 5,000-mile hike of the Eastern Continental
Trail, from Canada to Key West, Fla.  He says the insights he gained from the experience
led him to move to China and focus on his painting.  He works in acrylics and
oils on canvas in a colorful, contemporary style.
<br><br>
The theme of Ramasami\'s show in New York was "One World", with a philosophy of global brotherhood.
"Color is life, so my paintings are colorful," he says.  His paintings have been exhibited in India, Germany, Mexico, China and Japan in addition to the United States.
<br><br>
Sridhar Ramasami\'s colorful work includes "Peacock 2 (left) and "Dancers 2."
<br><br>


<br>
<b>Munchen Merkur September 4, 2010 - Munich, Germany</b>
<br><br>
<b>Translation:</b><br><br>
Art of three cultures<br>
V. Sridhar Ramasami painted between India, USA and China<br>
<br>
Martinsried - He has his roots in India, then went to the USA and now lives in Nanchang, China. The close relationship with the cultures of these three countries is reflected in Sridhar V. Ramasami\'s images. His art is unconventional. It reveals a romantic soul, expressed in a colorful and abstract way. "Passion Surprise" is the common title of twenty-two works that are currently in the foyer of the Martinsried Max Planck Institute. The exhibition is a Premier: Ramasami is in Germany for the first time.
<br><br>
The paintings have titles, but the pictures just come out spontaneously, says Sridhar V. Ramasami, Several show the relationship of husband and wife - such as "Couple 1" and "Couple 2". The images are painted in oil on canvas.  They artfully blend shapes and colors to depict emotions. If you look more closely at "Couple 1", you can see the traditional pattern of Indian saris, which are painted very precisely on the otherwise reduced body shapes. The circular heads are adorned with nose rings, and are a reminder of ancestors.
<br><br>
In China he learned to deal with tissue paper, says Ramasami. In his recent work he used it on the canvas to achieve a three-dimensional effect. The rhythm of Sridhar V Ramasami\'s color are harmonious, the technique impasto and artistic.
<br><br>
Ramasamis "Munich inspirations" stands out: they are acrylics and glossy gel.  These three images show lively cafe scenes and street musicians. "Munich is a vibrant and very inspiring city" says the artist. The inspirations has emerged in the early days of his stay in Bavaria. Overall Ramasamis images are worth seeing. They are unusual examples of contemporary and modern art and been exhibited in Japan, the United States, China, Mexico and India.
<br><br>
The exhibition "Passion Surprise" can be seen till the end of October in the foyer of the Max-Planck-Institute, Am Klopferspitz 18 Martinsired.
</p>';

	else $texts[] = '<p><strong>新闻</strong>
<br><br>


2010年9月4号，德国慕尼黑默克报报道
<br><br>
翻译:
<br><br>
受三种文化影响的艺术品
<br><br>
他出生在印度，然后去了美国，现在居住在中国南昌。这三种文化紧密的联系在Sridhar V. Ramasam的画中有所反应。他的作品是与众不同的，是用丰富多彩和抽象的方式表达了一种浪漫精神。“令人惊讶的激情”是目前22个艺术作品的共同主题，之前在他第一次去德国时，曾在德国普朗克马丁施瑞德马克思学院大厅展出。
<br><br>
每幅油画都有自己的标题，Sridhar V. Ramasami说“灵感是自发的”，有一些显示了丈夫和妻子的关系，例如“夫妻1”和“夫妻2”。这些画是用油料绘制在帆布上的，用颜色和形状来表达感情。如果你更接近的看“夫妻1”，你就可以看出印度的传统服饰莎丽服在画中精确地展现。在另一边减少了身体的形状，圆形的头上装饰了鼻环，这是对祖先的缅怀。
<br><br>
“在中国，他学着如何处理薄棉纸”Ramasami说，在他近期的作品中，他在帆布上运用它获得了三维效果，Sridhar V Ramasami画的节奏是和谐的，绘画技巧是高超而富有美感的。
<br><br>
Ramasami的“慕尼黑灵感”引人入胜处在于那些画是丙烯画且使用了有光泽的胶体。这三副图生动展示了咖啡场景和街头音乐家。艺术家说“慕尼黑是一个非常生动和启发灵感的城市”。灵感最早出现是在巴伐利亚州。所有的Ramasami的画都是值得欣赏的。它们是不同于近现代作品的典范，相继在日本、美国、中国、墨西哥和印度展出。
<br><br>
“令人惊讶的激情”展览将在马克思普朗克学院的前厅展出至10月底。
</p>';









		 $bottomButtonController = new BottomButtonController();
		$link = array();
		$text = array();
		 if($chin == 0) {
			$link[] = "aboutArtwork.php";
			$text[] = "About Artwork" ;
			$link[] = "series.php";
			$text[] = "See Artwork";
		} else {
			$link[] = "aboutArtwrk.php?chin=1";
			$text[] = "关于艺术";
			$link[] = "series.php?chin=1";
			$text[] = "艺术作品";
		}


		 $bottomButtonLinks = $bottomButtonController->getTexts($text, $link);
		 $data["bottomButtonLinks"] = $bottomButtonLinks;

		$emailSignup = new EmailSignupController();
		$emailSignupTexts = $emailSignup->getTexts();

		$contactSection = new ContactSectionController();
		$contactSectionTexts = $contactSection->getTexts();






		 //return View::make('painting')->with('paintings', compact($pageTitle, $data));

		 return View::make('aboutArtwork', compact('data', 'emailSignupTexts',
			'contactSectionTexts', 'texts'));
		 //return view('painting', ['title'=>$title, 'width'=>$width, 'height'=>$height]);
	}


	// ------------------------------------------------------------------------

}
