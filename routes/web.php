<?php


// Syncs all sales channels to pull in product updates to Stitch Lite.
Route::post('/api/sync', 'ScraperController@sync');
Route::get('/api/sync', 'ScraperController@sync');  //  used for testing, can be removed


// Returns all Stitch Lite products
Route::get('/api/products/{productId?}', 'ScraperController@products')->where('productId', '(.*)');


// Sync ups from Stitch Lite to Vend and Shopify
Route::post('/api/syncUp', 'ScraperController@syncUp');
Route::get('/api/syncUp', 'ScraperController@syncUp');  //  used for testing, can be removed


//



